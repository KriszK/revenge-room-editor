import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './../material/material.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EditorContainerComponent } from './components/editor-container/editor-container.component';
import { editorRouting } from './editor-routing.module';
import { GeneralEditorComponent } from './components/general-editor/general-editor.component';
import { ExitEditorComponent } from './components/exit-editor/exit-editor.component';
import { InspectEditorComponent } from './components/inspect-editor/inspect-editor.component';
import { NeedActionEditorComponent } from './components/need-action-editor/need-action-editor.component';
import { ObjectEditorComponent } from './components/object-editor/object-editor.component';
import { MainActionEditorComponent } from './components/main-action-editor/main-action-editor.component';
import { RoomNameDialogComponent } from './components/room-name-dialog/room-name-dialog.component';
import { ShowDetailsComponent } from './components/show-details/show-details.component';
import { ShowDetailsButtonComponent } from './components/show-details-button/show-details-button.component';
import { SearchDialogComponent } from './components/search-dialog/search-dialog.component';
import { ExitPanelComponent } from './components/exit-panel/exit-panel.component';
import { ExitPanelButtonComponent } from './components/exit-panel/exit-panel-button/exit-panel-button.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MaterialModule,
    editorRouting
  ],
  declarations: [
    EditorContainerComponent,
    GeneralEditorComponent,
    ExitEditorComponent,
    InspectEditorComponent,
    NeedActionEditorComponent,
    ObjectEditorComponent,
    MainActionEditorComponent,
    RoomNameDialogComponent,
    ShowDetailsButtonComponent,
    ShowDetailsComponent,
    SearchDialogComponent,
    ExitPanelComponent,
    ExitPanelButtonComponent,
  ],
  entryComponents: [
    RoomNameDialogComponent,
    ShowDetailsComponent,
    SearchDialogComponent
  ]
})
export class EditorModule { }
