import { MatDialog } from '@angular/material';
import { MainAction } from './../../models/revenge-config';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { RoomStoreService } from '../../services/room-store.service';
import { RoomNameDialogComponent } from '../room-name-dialog/room-name-dialog.component';
import { NewRoom, NewRoomFromAction } from '../../models/new-room';

@Component({
  selector: 'app-main-action-editor',
  templateUrl: './main-action-editor.component.html',
  styleUrls: ['./main-action-editor.component.scss']
})
export class MainActionEditorComponent {

  @Input() actions: Array<MainAction>;
  @Input() rooms: string[];
  @Input() selectedRoom: string;
  @Output() addRoom: EventEmitter<NewRoomFromAction> = new EventEmitter();

  constructor(private roomStore: RoomStoreService, private dialog: MatDialog) { }

  update(action: MainAction) {
    if (action && action.wining_location === 'add') {
      this.addNewRoom(action);
    }
    this.roomStore.setMainActions(this.selectedRoom, this.actions);
  }

  delete(index: number) {
    this.actions.splice(index, 1);
    if (this.actions.length === 0) {
      this.actions.push({} as MainAction);
    }
    this.roomStore.setMainActions(this.selectedRoom, this.actions);
  }

  add() {
    this.actions.push({
      name: null,
      message: null,
      action_id: null,
      need_action: null,
      need_items: null,
      item_event: null,
      wining_item: null,
      wining_location: null,
      remove_item: null,
      soundTrigger: null,
    });
  }

  private addNewRoom(action: MainAction) {
    const dialogRef = this.dialog.open(RoomNameDialogComponent);

    dialogRef.afterClosed().subscribe((newRoom: NewRoom) => {
      if (newRoom) {
        action.wining_location = newRoom.roomName;
        this.addRoom.emit({mainActions: this.actions, newRoom});
        }
    });
  }
}
