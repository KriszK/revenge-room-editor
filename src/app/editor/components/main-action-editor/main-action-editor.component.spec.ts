import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainActionEditorComponent } from './main-action-editor.component';

describe('MainActionEditorComponent', () => {
  let component: MainActionEditorComponent;
  let fixture: ComponentFixture<MainActionEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainActionEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainActionEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
