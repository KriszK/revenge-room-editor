import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectEditorComponent } from './inspect-editor.component';

describe('InspectEditorComponent', () => {
  let component: InspectEditorComponent;
  let fixture: ComponentFixture<InspectEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
