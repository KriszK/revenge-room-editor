import { Component, Input } from '@angular/core';
import { RoomStoreService } from '../../services/room-store.service';
import { Inspect } from './../../models/revenge-config';

@Component({
  selector: 'app-inspect-editor',
  templateUrl: './inspect-editor.component.html',
  styleUrls: ['./inspect-editor.component.scss']
})
export class InspectEditorComponent {

  @Input() inspects: Array<Inspect>;
  @Input() selectedRoom: string;

  constructor(private roomStore: RoomStoreService) { }

  update() {
    this.roomStore.setInspects(this.selectedRoom, this.inspects);
  }

  delete(index: number) {
    this.inspects.splice(index, 1);
    if (this.inspects.length === 0) {
      this.inspects.push({} as Inspect);
    }
    this.roomStore.setInspects(this.selectedRoom, this.inspects);
  }

  add(name?: string, message?: string) {
    const newInspect = {
      name,
      message
    };

    if (this.inspects.length === 1 && !this.inspects[0].name) {
      this.inspects = [newInspect];
    } else {
      this.inspects.push(newInspect);
    }
  }

  addEnvironment() {
    this.add('környezeted', 'Nem látsz semmi érdekeset.');
  }
}
