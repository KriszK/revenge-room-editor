import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeedActionEditorComponent } from './need-action-editor.component';

describe('NeedActionEditorComponent', () => {
  let component: NeedActionEditorComponent;
  let fixture: ComponentFixture<NeedActionEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeedActionEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeedActionEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
