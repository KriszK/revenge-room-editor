import { Component, Input, Output, EventEmitter } from '@angular/core';
import { NeedAction } from '../../models/revenge-config';
import { RoomStoreService } from '../../services/room-store.service';
import { NewRoomFromAction, NewRoom } from '../../models/new-room';
import { MatDialog } from '@angular/material';
import { RoomNameDialogComponent } from '../room-name-dialog/room-name-dialog.component';

@Component({
  selector: 'app-need-action-editor',
  templateUrl: './need-action-editor.component.html',
  styleUrls: ['./need-action-editor.component.scss']
})
export class NeedActionEditorComponent {

  @Input() actions: Array<NeedAction>;
  @Input() rooms: string[];
  @Input() selectedRoom: string;
  @Output() addRoom: EventEmitter<NewRoomFromAction> = new EventEmitter();

  constructor(private roomStore: RoomStoreService, private dialog: MatDialog) { }

  update(action?: NeedAction) {
    if (action && action.wining_location === 'add') {
      this.addNewRoom(action);
    }
    this.roomStore.setNeedActions(this.selectedRoom, this.actions);
  }

  delete(index: number) {
    this.actions.splice(index, 1);
    if (this.actions.length === 0) {
      this.add();
    }
    this.roomStore.setNeedActions(this.selectedRoom, this.actions);
  }

  add() {
    this.actions.push({
      need_id: null,
      wining_location: null,
    });
  }

  private addNewRoom(action: NeedAction) {
    const dialogRef = this.dialog.open(RoomNameDialogComponent);

    dialogRef.afterClosed().subscribe((newRoom: NewRoom) => {
      if (newRoom) {
        action.wining_location = newRoom.roomName;
        this.addRoom.emit({needActions: this.actions, newRoom});
        }
    });
  }

}
