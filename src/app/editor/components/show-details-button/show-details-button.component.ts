import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RevengeConfig } from '../../models/revenge-config';
import { RoomStoreService } from '../../services/room-store.service';
import { ShowDetailsComponent } from '../show-details/show-details.component';
import { JsonReaderService } from './../../services/json-reader.service';
import { ScrollStrategyOptions, Overlay } from '@angular/cdk/overlay';

@Component({
  selector: 'app-show-details-button',
  templateUrl: './show-details-button.component.html',
  styleUrls: ['./show-details-button.component.scss'],
})
export class ShowDetailsButtonComponent {
  @Input() config: RevengeConfig;

  constructor(
    private roomStore: RoomStoreService,
    private jsonReaderService: JsonReaderService,
    private overlay: Overlay,
    private dialog: MatDialog
  ) {}

  showDetails() {
    const generatedJson = this.jsonReaderService.convertRoomConfigToArray(this.roomStore.rooms);
    if (!this.config) {
      this.config = {} as RevengeConfig;
    }
    this.config.rooms = generatedJson;
    this.dialog.open(ShowDetailsComponent, {
      data: this.config,
      height: '80%',
      scrollStrategy: this.overlay.scrollStrategies.block()
    });
  }
}
