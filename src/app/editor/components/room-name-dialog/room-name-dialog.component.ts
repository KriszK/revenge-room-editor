import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NewRoom } from '../../models/new-room';

@Component({
  selector: 'app-room-name-dialog',
  templateUrl: './room-name-dialog.component.html',
  styleUrls: ['./room-name-dialog.component.scss']
})
export class RoomNameDialogComponent {

  formGroup: FormGroup;
  isCopy = false;

  constructor(public dialogRef: MatDialogRef<RoomNameDialogComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.formGroup = this.formBuilder.group({
      roomName: new FormControl('', Validators.required),
      description: new FormControl(''),
    });

    if(this.data) {
      this.isCopy = this.data.isCopy;
    }
   }

  cancel(): void {
    this.dialogRef.close();
  }

  create(): void {
    let dialogCloseData: NewRoom = {
      roomName: `room${this.formGroup.value.roomName}`
    };

    if (!this.isCopy) {
      dialogCloseData = {
        ...dialogCloseData,
        description: this.formGroup.value.description
      };
    }

    this.dialogRef.close(dialogCloseData);
  }
}
