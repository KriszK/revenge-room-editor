import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomNameDialogComponent } from './room-name-dialog.component';

describe('RoomNameDialogComponent', () => {
  let component: RoomNameDialogComponent;
  let fixture: ComponentFixture<RoomNameDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomNameDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomNameDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
