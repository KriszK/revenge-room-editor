import { Component, Input, OnChanges } from '@angular/core';
import { RoomStoreService } from '../../services/room-store.service';

@Component({
  selector: 'app-general-editor',
  templateUrl: './general-editor.component.html',
  styleUrls: ['./general-editor.component.scss']
})
export class GeneralEditorComponent implements OnChanges {

  @Input() alias: string;
  @Input() description: string;
  @Input() soundTrigger: string;
  @Input() chapterImage: string;
  @Input() room: string;

  results = [];
  isSearched = false;

  constructor(private roomStore: RoomStoreService) { }

  ngOnChanges() {
    this.isSearched = false;
  }

  changeAlias() {
    this.roomStore.setAlias(this.room, this.alias);
  }

  changeDescription() {
    this.roomStore.setDescription(this.room, this.description);
  }

  changeSoundTrigger() {
    this.roomStore.setSoundTrigger(this.room, this.soundTrigger);
  }

  changeChapterImage() {
    this.roomStore.setChapterImage(this.room, this.chapterImage);
  }

  search() {
    this.isSearched = true;
    this.results = this.roomStore.searchRoomByDescription(this.description, this.room);
  }
}
