import { ExitDirectionVM } from './../../models/exit-direction-vm';
import { Component, Input, OnInit } from '@angular/core';
import { Exit } from '../../models/revenge-config';
import { RoomStoreService } from '../../services/room-store.service';
import { exitDirections } from '../../constants/exit-directions';

@Component({
  selector: 'app-exit-editor',
  templateUrl: './exit-editor.component.html',
  styleUrls: ['./exit-editor.component.scss']
})
export class ExitEditorComponent implements OnInit {

  @Input() exit: Exit;
  @Input() rooms: string[];
  @Input() selectedRoom: string;

  firstColumn: ExitDirectionVM[];
  secondColumn: ExitDirectionVM[];
  thirdColumn: ExitDirectionVM[];

  constructor(private roomStore: RoomStoreService) { }

  ngOnInit() {
    this.firstColumn = exitDirections.slice(0, 4);
    this.secondColumn = exitDirections.slice(4, 8);
    this.thirdColumn = exitDirections.slice(8, 12);
  }

  updateExits() {
    this.exit = {
      ...this.exit
    };
    this.roomStore.setExit(this.selectedRoom, this.exit);
  }

}
