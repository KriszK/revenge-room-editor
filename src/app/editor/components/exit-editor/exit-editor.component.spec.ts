import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExitEditorComponent } from './exit-editor.component';

describe('ExitEditorComponent', () => {
  let component: ExitEditorComponent;
  let fixture: ComponentFixture<ExitEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExitEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExitEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
