import { NewRoomFromAction } from './../../models/new-room';
import { ExitDirectionService } from './../../services/exit-direction.service';
import { SearchDialogComponent } from './../search-dialog/search-dialog.component';
import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { saveAs } from 'file-saver';
import { RevengeConfig, RoomConfig } from '../../models/revenge-config';
import { JsonReaderService } from '../../services/json-reader.service';
import { RoomStoreService } from '../../services/room-store.service';
import { RoomNameDialogComponent } from '../room-name-dialog/room-name-dialog.component';
import { EmptyRoomGeneratorService } from './../../services/empty-room-generator.service';
import { NewRoom, NewRoomWithDirection } from '../../models/new-room';

@Component({
  selector: 'app-editor-container',
  templateUrl: './editor-container.component.html',
  styleUrls: ['./editor-container.component.scss'],
})
export class EditorContainerComponent {
  rooms: RoomConfig = {};
  roomKeys: string[] = [];
  selectedRoom: string;
  generatedJson: Array<RoomConfig>;
  file: any;
  config: any;
  isDescriptionVisible = true;

  constructor(
    private jsonReaderService: JsonReaderService,
    private roomStore: RoomStoreService,
    private dialog: MatDialog,
    private roomGenerator: EmptyRoomGeneratorService,
    private exitDirectionService: ExitDirectionService
  ) { }

  load(fileInputEvent: any) {
    this.file = fileInputEvent.target.files[0];
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.config = JSON.parse(fileReader.result.toString());

      this.rooms = this.jsonReaderService.convertRoomsToRoomConfig(this.config.rooms);
      this.roomStore.rooms = this.rooms;
      this.sortRooms();
      this.selectedRoom = this.roomKeys[0];
    };
    fileReader.readAsText(this.file);
  }

  private sortRooms() {
    this.roomKeys = Object.keys(this.rooms).sort((a, b) => {
      return Number(a.substring(4)) - Number(b.substring(4));
    });
  }

  save() {
    this.generatedJson = this.jsonReaderService.convertRoomConfigToArray(this.roomStore.rooms);
    if (!this.config) {
      this.config = {} as RevengeConfig;
    }
    this.config.rooms = this.generatedJson;
    const file = new Blob([JSON.stringify(this.config, null, '\t')], { type: 'application/json' });
    const fileName = 'rooms.json';
    saveAs(file, fileName);
  }

  addRoom() {
    const addRoom = (roomName: string, description: string) => this.rooms[roomName] = this.roomGenerator.create(description);
    this.openRoomNameDialog(addRoom);
  }


  copyRoom() {
    const addRoom = (roomName: string) => this.rooms[roomName] = this.roomStore.rooms[this.selectedRoom];
    this.openRoomNameDialog(addRoom, true);
  }

  private openRoomNameDialog(addRoom: Function, isCopy = false) {
    const dialogRef = this.dialog.open(RoomNameDialogComponent, {
      data: {
        isCopy
      }
    });

    dialogRef.afterClosed().subscribe((newRoom: NewRoom) => {
      if (newRoom && newRoom.roomName) {
        addRoom(newRoom.roomName, newRoom.description);
        this.addNewRoom(newRoom.roomName);
      }
    });
  }

  deleteRoom() {
    delete this.rooms[this.selectedRoom];
    const roomToDeleteIndex = this.roomKeys.indexOf(this.selectedRoom);
    this.roomKeys.splice(roomToDeleteIndex, 1);
    this.selectedRoom = this.roomKeys[0];
  }

  searchRoom() {
    const dialogRef = this.dialog.open(SearchDialogComponent);

    dialogRef.afterClosed().subscribe((roomName: string) => {
      if (roomName) {
        this.selectedRoom = roomName;
      }
    });
  }

  onRoomSelect(event) {
    this.selectedRoom = event;
  }

  addNewRoomFromAction(room: NewRoomFromAction) {
    if (room.mainActions) {
      this.rooms[this.selectedRoom].mainActions = [...room.mainActions];
    } else {
      this.rooms[this.selectedRoom].needAction = [...room.needActions];
    }

    this.rooms[room.newRoom.roomName] = this.roomGenerator.create(room.newRoom.description);
    this.addNewRoom(room.newRoom.roomName);
  }

  addNewRoomFromExitPanel(newRoom: NewRoomWithDirection) {
    this.setNewExitOnSelectedRoom(newRoom.direction, newRoom.roomName);
    this.rooms[newRoom.roomName] = this.roomGenerator.create(newRoom.description);
    this.addNewRoom(newRoom.roomName);
  }

  private addNewRoom(roomName: string) {
    this.roomStore.rooms = this.rooms;

    this.roomKeys.push(roomName);
    this.sortRooms();

    this.selectedRoom = roomName;
  }

  private setNewExitOnSelectedRoom(direction: string, exit: string) {
    const property = this.exitDirectionService.getPropertyFromDirection(direction);
    const room = this.rooms[this.selectedRoom];
    room.exits[property] = exit;
  }

  toggleDescription() {
    this.isDescriptionVisible = !this.isDescriptionVisible;
  }
}

