import { Component } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { RoomStoreService } from '../../services/room-store.service';

@Component({
  selector: 'app-search-dialog',
  templateUrl: './search-dialog.component.html',
  styleUrls: ['./search-dialog.component.scss']
})
export class SearchDialogComponent {

  formGroup: FormGroup;
  results = [];
  isSearched = false;

  constructor(
    public dialogRef: MatDialogRef<SearchDialogComponent>,
    private formBuilder: FormBuilder,
    private roomStore: RoomStoreService,
  ) {
    this.formGroup = this.formBuilder.group({
      description: new FormControl('')
    });
   }

  search() {
    this.isSearched = true;
    this.results = this.roomStore.searchRoomByDescription(this.formGroup.value.description);
  }

  cancel(): void {
    this.dialogRef.close();
  }

  openRoom(): void {
    const room = this.results.length === 1 ? this.results[0] : undefined;
    this.dialogRef.close(room);
  }

}
