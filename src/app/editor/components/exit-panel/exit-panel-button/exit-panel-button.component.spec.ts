import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExitPanelButtonComponent } from './exit-panel-button.component';

describe('ExitPanelButtonComponent', () => {
  let component: ExitPanelButtonComponent;
  let fixture: ComponentFixture<ExitPanelButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExitPanelButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExitPanelButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
