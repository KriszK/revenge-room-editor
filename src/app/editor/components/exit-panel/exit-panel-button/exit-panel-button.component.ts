import { MatDialog } from '@angular/material';
import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { RoomNameDialogComponent } from '../../room-name-dialog/room-name-dialog.component';
import { NewRoom, NewRoomWithDirection } from '../../../models/new-room';

@Component({
  selector: 'app-exit-panel-button',
  templateUrl: './exit-panel-button.component.html',
  styleUrls: ['./exit-panel-button.component.scss']
})
export class ExitPanelButtonComponent implements OnChanges {

  @Input() title: string;
  @Output() selectRoom: EventEmitter<string> = new EventEmitter<string>();
  @Output() addNewRoom: EventEmitter<NewRoomWithDirection> = new EventEmitter<NewRoomWithDirection>();

  isDisabled = false;

  constructor(private dialog: MatDialog) { }

  ngOnChanges() {
    if (this.title) {
      this.isDisabled = !this.title.startsWith('room');
    }
  }

  onRoomSelect() {
    this.selectRoom.emit(this.title);
  }

  addRoom() {
    const dialogRef = this.dialog.open(RoomNameDialogComponent);

    dialogRef.afterClosed().subscribe((newRoom: NewRoom) => {
      if (newRoom) {
        this.addNewRoom.emit({
          ...newRoom,
          direction: this.title

        });
       }
    });
  }

}
