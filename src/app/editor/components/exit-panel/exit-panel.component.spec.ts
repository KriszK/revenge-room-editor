import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExitPanelComponent } from './exit-panel.component';

describe('ExitPanelComponent', () => {
  let component: ExitPanelComponent;
  let fixture: ComponentFixture<ExitPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExitPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExitPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
