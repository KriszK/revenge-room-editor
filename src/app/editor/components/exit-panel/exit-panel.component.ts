import { MainAction } from './../../models/revenge-config';
import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { Exit, NeedAction } from '../../models/revenge-config';
import { NewRoom, NewRoomWithDirection } from '../../models/new-room';
import { ExitDirectionProperty, ExitDirection } from '../../enums/exit-direction';

@Component({
  selector: 'app-exit-panel',
  templateUrl: './exit-panel.component.html',
  styleUrls: ['./exit-panel.component.scss']
})
export class ExitPanelComponent implements OnChanges {

  @Input() exit: Exit;
  @Input() needActions: NeedAction[];
  @Input() mainActions: MainAction[];

  @Output() selectRoom: EventEmitter<string> = new EventEmitter<string>();
  @Output() addNewRoom: EventEmitter<NewRoomWithDirection> = new EventEmitter<NewRoomWithDirection>();

  exitCardinals: string[][];
  exitDirections: string[][];
  exitActions: {action?: string, exitRoom: string}[];

  constructor() { }

  ngOnChanges() {
    this.setExitCardinals();
    this.setExitDirections();
    this.setExitActions();
  }

  private setExitCardinals() {
    const north = this.isDirectionExists(this.exit[ExitDirectionProperty.NORTH]) ? this.exit[ExitDirectionProperty.NORTH] : ExitDirection.NORTH;
    const nW = this.isDirectionExists(this.exit[ExitDirectionProperty.NW]) ? this.exit[ExitDirectionProperty.NW] : ExitDirection.NW;
    const nE = this.isDirectionExists(this.exit[ExitDirectionProperty.NE]) ? this.exit[ExitDirectionProperty.NE] : ExitDirection.NE;
    const west = this.isDirectionExists(this.exit[ExitDirectionProperty.WEST]) ? this.exit[ExitDirectionProperty.WEST] : ExitDirection.WEST;
    const east = this.isDirectionExists(this.exit[ExitDirectionProperty.EAST]) ? this.exit[ExitDirectionProperty.EAST] : ExitDirection.EAST;
    const sW = this.isDirectionExists(this.exit[ExitDirectionProperty.SW]) ? this.exit[ExitDirectionProperty.SW] : ExitDirection.SW;
    const sE = this.isDirectionExists(this.exit[ExitDirectionProperty.SE]) ? this.exit[ExitDirectionProperty.SE] : ExitDirection.SE;
    const south = this.isDirectionExists(this.exit[ExitDirectionProperty.SOUTH]) ? this.exit[ExitDirectionProperty.SOUTH] : ExitDirection.SOUTH;

    this.exitCardinals = [
      [null, null, north, null, null],
      [null, nW,   null,  nE,   null],
      [west, null, null,  null, east],
      [null, sW,   null,  sE,   null],
      [null, null, south, null, null]
    ];
  }

  private setExitDirections() {
    const up = this.isDirectionExists(this.exit[ExitDirectionProperty.UP]) ? this.exit[ExitDirectionProperty.UP] : ExitDirection.UP;
    const down = this.isDirectionExists(this.exit[ExitDirectionProperty.DOWN]) ? this.exit[ExitDirectionProperty.DOWN] : ExitDirection.DOWN;
    const outD = this.isDirectionExists(this.exit[ExitDirectionProperty.OUT]) ? this.exit[ExitDirectionProperty.OUT] : ExitDirection.OUT;
    const inD = this.isDirectionExists(this.exit[ExitDirectionProperty.IN]) ? this.exit[ExitDirectionProperty.IN] : ExitDirection.IN;

    this.exitDirections = [
      [null, up, null],
      [outD, null, inD],
      [null, down, null]
    ];
  }

  private setExitActions() {
    const needActionsWithExit = this.needActions
      .filter(this.hasExit)
      .map((action: NeedAction) => {
        return {
          exitRoom: action.wining_location
        };
      });

    const mainActionsWithExit = this.mainActions
      .filter(this.hasExit)
      .map((action: MainAction) => {
        return {
          action: action.name,
          exitRoom: action.wining_location
        };
      });

    this.exitActions = [...needActionsWithExit, ...mainActionsWithExit];
  }

  private isDirectionExists(direction: string): boolean {
    return direction !== '-1';
  }

  private hasExit(action: MainAction | NeedAction) {
    return action.wining_location !== null && action.wining_location !== undefined;
  }

  onRoomSelect(event) {
    this.selectRoom.emit(event);
  }

  addRoom(newRoom: NewRoomWithDirection) {
    this.addNewRoom.emit(newRoom);
  }

}
