import { Component, Input } from '@angular/core';
import { RevengeObject } from '../../models/revenge-config';
import { RoomStoreService } from '../../services/room-store.service';

@Component({
  selector: 'app-object-editor',
  templateUrl: './object-editor.component.html',
  styleUrls: ['./object-editor.component.scss']
})
export class ObjectEditorComponent {

  @Input() objects: Array<RevengeObject>;
  @Input() rooms: string[];
  @Input() selectedRoom: string;

  constructor(private roomStore: RoomStoreService) { }

  update() {
    this.roomStore.setObjects(this.selectedRoom, this.objects);
  }

  delete(index: number) {
    this.objects.splice(index, 1);
    if (this.objects.length === 0) {
      this.objects.push({} as RevengeObject);
    }
    this.roomStore.setObjects(this.selectedRoom, this.objects);
  }

  add() {
    this.objects.push({
      name: null,
    });
  }
}
