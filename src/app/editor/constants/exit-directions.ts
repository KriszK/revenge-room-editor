import { ExitDirectionVM } from '../models/exit-direction-vm';
import { ExitDirectionProperty } from '../enums/exit-direction';

export const exitDirections: ExitDirectionVM[] = [{
  label: 'North',
  direction: ExitDirectionProperty.NORTH
}, {
  label: 'South',
  direction: ExitDirectionProperty.SOUTH
}, {
  label: 'East',
  direction: ExitDirectionProperty.EAST
}, {
  label: 'West',
  direction: ExitDirectionProperty.WEST
}, {
  label: 'North-east',
  direction: ExitDirectionProperty.NE
}, {
  label: 'North-west',
  direction: ExitDirectionProperty.NW
}, {
  label: 'South-east',
  direction: ExitDirectionProperty.SE
}, {
  label: 'South-west',
  direction: ExitDirectionProperty.SW
}, {
  label: 'Up',
  direction: ExitDirectionProperty.UP
}, {
  label: 'Down',
  direction: ExitDirectionProperty.DOWN
}, {
  label: 'In',
  direction: ExitDirectionProperty.IN
}, {
  label: 'Out',
  direction: ExitDirectionProperty.OUT
}] ;
