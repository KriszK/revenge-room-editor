import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditorContainerComponent } from './components/editor-container/editor-container.component';

export const routes: Routes = [
  { path: '', component: EditorContainerComponent }
];

export const editorRouting: ModuleWithProviders = RouterModule.forChild(routes);
