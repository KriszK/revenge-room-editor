import { ExitDirectionVM } from './../models/exit-direction-vm';
export enum ExitDirectionProperty {
  NORTH = 'b_north',
  NW = 'b_nortwest',
  NE = 'b_northeast',
  WEST = 'b_west',
  EAST = 'b_east',
  SW = 'b_southwest',
  SE = 'b_eastsouth',
  SOUTH = 'b_south',
  UP = 'b_up',
  DOWN = 'b_down',
  OUT = 'b_out',
  IN = 'b_in'
}

export enum ExitDirection {
  NORTH = 'N',
  NW = 'NW',
  NE = 'NE',
  WEST = 'W',
  EAST = 'E',
  SW = 'SW',
  SE = 'SE',
  SOUTH = 'S',
  UP = 'UP',
  DOWN = 'DOWN',
  OUT = 'OUT',
  IN = 'IN'
}
