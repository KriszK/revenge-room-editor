import { ExitDirectionProperty } from './../enums/exit-direction';

export interface ExitDirectionVM {
  label: string;
  direction: ExitDirectionProperty;
}
