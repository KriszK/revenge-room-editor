
export interface RevengeConfig {
  name: string;
  meta: any;
  startItem: Array<StartItem>;
  allActions: Array<String>;
  rooms: Array<RoomConfig>;
}

interface StartItem {
  name: string;
}

export interface RoomConfig {
  [key: string]: Room;
}

export interface Room {
  alias: string;
  needAction: Array<NeedAction>;
  description: string;
  mainActions: Array<MainAction | {}>;
  inspect: Array<Inspect | {}>;
  exits: Exit;
  objects: Array<RevengeObject | {}>;
  enemies: any;
  soundTrigger?: string;
  chapterImage?: string;
}

export interface NeedAction {
  need_id: number;
  wining_location: string;
  message?: string;
}

export interface MainAction {
  name: string;
  message: string;
  action_id: number;
  need_action: number;
  need_items: string;
  item_event: string;
  wining_item: string;
  wining_location: string;
  remove_item: string;
  soundTrigger?: string;
}

export interface Inspect {
  name: string;
  message: string;
}

export interface Exit {
  b_north: string;
  b_east: string;
  b_south: string;
  b_west: string;
  b_northeast: string;
  b_eastsouth: string;
  b_southwest: string;
  b_nortwest: string;
  b_up: string;
  b_down: string;
  b_in: string;
  b_out: string;
}

export interface RevengeObject {
  name: string;
}
