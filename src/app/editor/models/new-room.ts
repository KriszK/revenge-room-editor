import { MainAction, NeedAction } from './revenge-config';

export interface NewRoom {
  roomName: string;
  description?: string;
}

export interface NewRoomWithDirection {
  roomName: string;
  description?: string;
  direction: string;
}

export interface NewRoomFromAction {
  mainActions?: MainAction[];
  needActions?: NeedAction[];
  newRoom: NewRoom;
}
