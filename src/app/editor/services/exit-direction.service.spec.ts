import { TestBed, inject } from '@angular/core/testing';

import { ExitDirectionService } from './exit-direction.service';

describe('ExitDirectionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExitDirectionService]
    });
  });

  it('should be created', inject([ExitDirectionService], (service: ExitDirectionService) => {
    expect(service).toBeTruthy();
  }));
});
