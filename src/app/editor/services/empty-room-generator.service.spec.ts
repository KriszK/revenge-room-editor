import { TestBed, inject } from '@angular/core/testing';

import { EmptyRoomGeneratorService } from './empty-room-generator.service';

describe('EmptyRoomGeneratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmptyRoomGeneratorService]
    });
  });

  it('should be created', inject([EmptyRoomGeneratorService], (service: EmptyRoomGeneratorService) => {
    expect(service).toBeTruthy();
  }));
});
