import { ExitDirection } from './../enums/exit-direction';
import { Injectable } from '@angular/core';
import { ExitDirectionProperty } from '../enums/exit-direction';

@Injectable({
  providedIn: 'root'
})
export class ExitDirectionService {

  getPropertyFromDirection(direction: string): string {
    let property = '';

    switch (direction) {
      case ExitDirection.NORTH:
        property = ExitDirectionProperty.NORTH;
        break;
      case ExitDirection.NW:
        property = ExitDirectionProperty.NW;
        break;
      case ExitDirection.NE:
        property = ExitDirectionProperty.NE;
        break;
      case ExitDirection.WEST:
        property = ExitDirectionProperty.WEST;
        break;
      case ExitDirection.EAST:
        property = ExitDirectionProperty.EAST;
        break;
      case ExitDirection.SW:
        property = ExitDirectionProperty.SW;
        break;
      case ExitDirection.SE:
        property = ExitDirectionProperty.SE;
        break;
      case ExitDirection.SOUTH:
        property = ExitDirectionProperty.SOUTH;
        break;
      case ExitDirection.UP:
        property = ExitDirectionProperty.UP;
        break;
      case ExitDirection.DOWN:
        property = ExitDirectionProperty.DOWN;
        break;
      case ExitDirection.OUT:
        property = ExitDirectionProperty.OUT;
        break;
      case ExitDirection.IN:
        property = ExitDirectionProperty.IN;
        break;
      default:
        break;
    }

    return property;
  }
}
