import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EmptyRoomGeneratorService {

  constructor() { }

  create(description?: string) {
    return {
      alias: null,
      needAction: [{
        need_id: null,
        wining_location: null
      }],
      description: description ? description : null,
      mainActions: [{}],
      inspect: [{}],
      exits: {
        b_north: '-1',
        b_east: '-1',
        b_south: '-1',
        b_west: '-1',
        b_northeast: '-1',
        b_eastsouth: '-1',
        b_southwest: '-1',
        b_nortwest: '-1',
        b_up: '-1',
        b_down: '-1',
        b_in: '-1',
        b_out: '-1',
      },
      objects: [{}],
      enemies: null,
    };
  }
}
