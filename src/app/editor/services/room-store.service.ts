import { MainAction } from './../models/revenge-config';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { RoomConfig, Exit, Inspect, NeedAction, RevengeObject } from '../models/revenge-config';

@Injectable({
  providedIn: 'root'
})
export class RoomStoreService {

  private _rooms = new BehaviorSubject<RoomConfig>(null);
  rooms$: Observable<RoomConfig> = this._rooms.asObservable();

  get rooms(): RoomConfig {
    return this._rooms.getValue();
  }

  set rooms(value: RoomConfig) {
    this._rooms.next(value);
  }

  setAlias(key: string, alias: string) {
    this.rooms[key].alias = alias;
  }

  setDescription(key: string, description: string) {
    this.rooms[key].description = description;
  }

  setSoundTrigger(key: string, soundTrigger: string) {
    this.rooms[key].soundTrigger = soundTrigger;
  }

  setChapterImage(key: string, chapterImage: string) {
    this.rooms[key].chapterImage = chapterImage;
  }

  setNeedActions(key: string, actions: Array<NeedAction>) {
    this.rooms[key].needAction = actions;
  }

  setMainActions(key: string, actions: Array<MainAction>) {
    this.rooms[key].mainActions = actions;
  }

  setInspects(key: string, inspects: Array<Inspect>) {
    this.rooms[key].inspect = inspects;
  }

  setExit(key: string, exit: Exit) {
    this.rooms[key].exits = exit;
  }

  setObjects(key: string, objects: Array<RevengeObject>) {
    this.rooms[key].objects = objects;
  }

  searchRoomByDescription(description: string, omitRoom?: string): string[]  {
    const result = [];

    if (this.rooms) {
      Object.keys(this.rooms).forEach(key => {
        if (key !== omitRoom) {

          const desc = this.rooms[key].description;
          if (desc.includes(description)) {
            result.push(key);
          }
        }
      });
    }
    return result;
  }
}
