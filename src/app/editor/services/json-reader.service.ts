import { Injectable } from '@angular/core';
import { RoomConfig, Room, MainAction } from '../models/revenge-config.js';

@Injectable({
  providedIn: 'root',
})
export class JsonReaderService {
  private mainActionProperties = ['name', 'message', 'action_id', 'need_action', 'need_items', 'item_event', 'wining_item', 'wining_location:', 'remove_item', 'soundTrigger'];

  constructor() {}

  convertRoomsToRoomConfig(rooms: Array<RoomConfig>): RoomConfig {
    const result: RoomConfig = {};

    rooms.forEach((room: RoomConfig) => {
      const key = Object.keys(room)[0];
      result[key] = room[key];
    });

    return result;
  }

  convertRoomConfigToArray(rooms: RoomConfig): Array<RoomConfig> {
    const result = [];

    Object.keys(rooms).forEach((key: string) => {
      const room = {};
      room[key] = rooms[key];
      this.checkMainActions(room, key);
      result.push(room);
    });

    return result;
  }

  private checkMainActions(room: any, key: string): void {
    if (Object.keys(room[key].mainActions[0]).length === 0) {
      return;
    }

    room[key].mainActions.map((action: MainAction) => {
      const actualKeys = Object.keys(action);
      this.mainActionProperties.forEach((property: string) => {
        if (actualKeys.indexOf(property) < 0) {
          action[property] = null;
        }
      });
    });
  }


}
